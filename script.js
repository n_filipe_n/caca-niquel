const main = document.createElement('div');
main.id = 'main';
document.body.appendChild(main);

const roulettes__main = document.createElement('div');
roulettes__main.id = 'roulettes';
main.appendChild(roulettes__main);

const roul_um_roulettes = document.createElement('div');
roul_um_roulettes.className = 'panel';
roulettes__main.appendChild(roul_um_roulettes);

const roul_dois_roulettes = document.createElement('div');
roul_dois_roulettes.className = 'panel';
roulettes__main.appendChild(roul_dois_roulettes);

const roul_tres_roulettes = document.createElement('div');
roul_tres_roulettes.className = 'panel';
roulettes__main.appendChild(roul_tres_roulettes);

const div_p_main = document.createElement('div');
div_p_main.id = 'div_p_main'
main.appendChild(div_p_main);

const p__main = document.createElement('p');
p__main.id = 'p__main'
div_p_main.appendChild(p__main);

const div__button = document.createElement('div');
div__button.id = 'div__button'
main.appendChild(div__button);

const winner = document.createElement('div');
winner.className = 'points'
div__button.appendChild(winner);

let h2__winner = document.createElement('h2');
h2__winner.innerText = 'VITÓRIAS';
h2__winner.className = 'h2__points'
winner.appendChild(h2__winner)

const points__winner = document.createElement('span');
points__winner.className = 'div__points'
points__winner.innerText = 0;
winner.appendChild(points__winner);

const button__main = document.createElement('button');
button__main.id = 'button';
button__main.innerText = 'START';
div__button.appendChild(button__main);

const lose = document.createElement('div');
lose.className = 'points'
div__button.appendChild(lose);

let h2__lose = document.createElement('h2');
h2__lose.innerText = 'DERROTAS';
h2__lose.className = 'h2__points';
lose.appendChild(h2__lose);

const points__lose = document.createElement('span');
points__lose.className = 'div__points'
points__lose.innerText = 0;
lose.appendChild(points__lose);

const randomico = () => {
    return Math.floor(Math.random() * (4 - 1) + 1);
}
let countWinner = 0
let countLose = 0
const victoryCondition = (a, b, c) => {

    if (a === b && a === c) {
        countWinner++
        points__winner.innerText = countWinner;
        console.log('ganhou')
        p__main.innerText = 'Parabéns! você ganhou!';
    } else {
        countLose++
        points__lose.innerText = countLose;
        console.log('perdeu')
        p__main.innerText = 'Você perdeu, mais sorte na próxima!';
    }
}
const start = () => {
    roul_um_roulettes.innerText = randomico();
    roul_dois_roulettes.innerText = randomico();
    roul_tres_roulettes.innerText = randomico();

    victoryCondition(roul_um_roulettes.innerText, roul_dois_roulettes.innerText, roul_tres_roulettes.innerText);

    button__main.innerText = 'RESET';
    button__main.addEventListener('click', reset);
};
const reset = () => {
    roul_um_roulettes.innerText = '';
    roul_dois_roulettes.innerText = '';
    roul_tres_roulettes.innerText = '';
    p__main.innerText = '';
    button__main.innerText = 'START';
    button__main.removeEventListener('click', reset);
    button__main.addEventListener('click', start);
}
button__main.addEventListener('click', start);